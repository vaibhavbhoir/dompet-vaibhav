$(document).ready(function() {
    $('#selectAll').click(function(e){        
        $(this).closest('tbody').find('td input:checkbox').prop('checked', this.checked);
        console.log("checked");
    });
});

// three dot clicked script 
function showDropdown() {
    document.getElementById("myDropdown").classList.toggle("show");
    console.log("log")
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
    if (!event.target.matches('.threedot')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
}

// date range picker 
$(function() {
    $('input[name="birthday"]').daterangepicker({
      singleDatePicker: true,
      showDropdowns: true,
      minYear: 1901,
      maxYear: parseInt(moment().format('YYYY'),10)
    });
  });

//   select deselect checkbox 
$("#selectAll").change(function () {
    $("input:checkbox").prop('checked', $(this).prop("checked"));
  });

  



// dashboard plus my wallet doughnut chart 
google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Work',     11],
          ['Eat',      2],
          ['Commute',  2],
          ['Watch TV', 2],
          ['Sleep',    7]
        ]);

        var options = {
          title: 'My Daily Activities',
          pieHole: 0.4,
        };

        var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
        chart.draw(data, options);
      }

// bar chart dashboard
var ctx = document.getElementById('bar').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor:  ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
                
            borderColor:  ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
                
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

// doughnut chart dashboard

var ctx = document.getElementById('mydoughnut').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 7],
            backgroundColor:  ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
                
            borderColor:  ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
                
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

// my wallet curve chart 
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawSeriesChart);

function drawSeriesChart() {

var data = google.visualization.arrayToDataTable([
  ['ID', 'Life Expectancy', 'Fertility Rate', 'Region',     'Population'],
  ['CAN',    80.66,              1.67,      'North America',  33739900],
  ['DEU',    79.84,              1.36,      'Europe',         81902307],
  ['DNK',    78.6,               1.84,      'Europe',         5523095],
  ['EGY',    72.73,              2.78,      'Middle East',    79716203],
  ['GBR',    80.05,              2,         'Europe',         61801570],
  ['IRN',    72.49,              1.7,       'Middle East',    73137148],
  ['IRQ',    68.09,              4.77,      'Middle East',    31090763],
  ['ISR',    81.55,              2.96,      'Middle East',    7485600],
  ['RUS',    68.6,               1.54,      'Europe',         141850000],
  ['USA',    78.09,              2.05,      'North America',  307007000]
]);

var options = {
  title: 'Correlation between life expectancy, fertility rate ' +
         'and population of some world countries (2010)',
  hAxis: {title: 'Life Expectancy'},
  vAxis: {title: 'Fertility Rate'},
  bubble: {textStyle: {fontSize: 11}}
};

var chart = new google.visualization.BubbleChart(document.getElementById('series_chart_div'));
chart.draw(data, options);
}

// select all table 
$(document).ready(function() {
    var $selectAll = $('#selectAll'); // main checkbox inside table thead
    var $table = $('.table'); // table selector 
    var $tdCheckbox = $table.find('tbody input:checkbox'); // checboxes inside table body
    var tdCheckboxChecked = 0; // checked checboxes
  
    // Select or deselect all checkboxes depending on main checkbox change
    $selectAll.on('click', function () {
      $tdCheckbox.prop('checked', this.checked);
    });
  
    // Toggle main checkbox state to checked when all checkboxes inside tbody tag is checked
    $tdCheckbox.on('change', function(e){
      tdCheckboxChecked = $table.find('tbody input:checkbox:checked').length; // Get count of checkboxes that is checked
      // if all checkboxes are checked, then set property of main checkbox to "true", else set to "false"
      $selectAll.prop('checked', (tdCheckboxChecked === $tdCheckbox.length));
    })
  });


//   card-center graph 
// google.charts.load('current', {'packages':['corechart']});
// google.charts.setOnLoadCallback(drawChart);

// function drawChart() {

//   var data = new google.visualization.DataTable();
//   data.addColumn('string', 'Pizza');
//   data.addColumn('number', 'Populartiy');
//   data.addRows([
//     ['Pepperoni', 33],
//     ['Hawaiian', 26],
//     ['Mushroom', 22],
//     ['Sausage', 10], // Below limit.
//     ['Anchovies', 9] // Below limit.
//   ]);

//   var options = {
//     title: 'Popularity of Types of Pizza',
//     sliceVisibilityThreshold: .2
//   };

//   var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
//   chart.draw(data, options);
// }


  





